# facenet适配昇腾npu

项目源地址：https://github.com/timesler/facenet-pytorch

相关环境依赖安装参考：https://bbs.huaweicloud.com/blogs/442414

infer_cn_terminal：推理代码，源码用的是jupyter，修改部分代码，在终端交互，指定npu加速模块

face_tracking_cn_terminal：实时追踪代码，源码用的是jupyter，修改部分代码，在终端交互，指定npu加速模块

