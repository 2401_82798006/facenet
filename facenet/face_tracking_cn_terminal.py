from facenet_pytorch import MTCNN
import torch
import torch_npu
import numpy as np
import mmcv, cv2
from PIL import Image, ImageDraw

device = torch.device('npu:0' if torch_npu.npu.is_available() else 'cpu')
print('在该设备上运行: {}'.format(device))

mtcnn = MTCNN(keep_all=True, device=device)

video = mmcv.VideoReader('video.mp4')
frames = [Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)) for frame in video]

frames_tracked = []
for i, frame in enumerate(frames):
    print('\r当前帧: {}'.format(i + 1), end='')
    
    # 检测人脸
    boxes, _ = mtcnn.detect(frame)
    
    # 绘制人脸框
    frame_draw = frame.copy()
    draw = ImageDraw.Draw(frame_draw)
    if boxes is not None:
        for box in boxes:
            draw.rectangle(box.tolist(), outline=(255, 0, 0), width=6)
    
    # 添加到图像列表
    frames_tracked.append(frame_draw.resize((640, 360), Image.BILINEAR))
print('\n结束')


dim = frames_tracked[0].size
fourcc = cv2.VideoWriter_fourcc(*'mp4v')    
video_tracked = cv2.VideoWriter('video_tracked.mp4', fourcc, 25.0, dim)
for frame in frames_tracked:
    video_tracked.write(cv2.cvtColor(np.array(frame), cv2.COLOR_RGB2BGR))
video_tracked.release()
