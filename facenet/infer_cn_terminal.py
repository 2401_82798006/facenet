import os
from facenet_pytorch import MTCNN, InceptionResnetV1
import torch
import torch_npu
from torch.utils.data import DataLoader
from torchvision import datasets
import numpy as np
import pandas as pd

def main():
    # 设置工作线程数量
    workers = 0 if os.name == 'nt' else 4

    # 设置设备为NPU，如果不可用则使用CPU
    device = torch.device('npu:0' if torch_npu.npu.is_available() else 'cpu')
    print('在该设备上运行: {}'.format(device))

    # 初始化MTCNN人脸检测模型
    mtcnn = MTCNN(
        image_size=160, margin=0, min_face_size=20,
        thresholds=[0.6, 0.7, 0.7], factor=0.709, post_process=True,
        device=device
    )

    # 初始化InceptionResnetV1人脸识别模型
    resnet = InceptionResnetV1(pretrained='vggface2').eval().to(device)

    # 自定义数据加载函数
    def collate_fn(x):
        return x[0]

    # 加载图像数据集
    data_path = '../data/test_images'
    if not os.path.exists(data_path):
        print(f'数据目录不存在: {data_path}')
        return

    dataset = datasets.ImageFolder(data_path)
    dataset.idx_to_class = {i: c for c, i in dataset.class_to_idx.items()}
    loader = DataLoader(dataset, collate_fn=collate_fn, num_workers=workers)

    aligned = []
    names = []
    for x, y in loader:
        x_aligned, prob = mtcnn(x, return_prob=True)
        if x_aligned is not None:
            print('检测到的人脸及其概率: {:8f}'.format(prob))
            if isinstance(x_aligned, torch.Tensor):
                aligned.append(x_aligned)
                names.append(dataset.idx_to_class[y])

    if len(aligned) == 0:
        print('没有检测到任何人脸！')
    else:
        aligned = torch.stack(aligned).to(device)
        embeddings = resnet(aligned).detach().to(device)          

        # 计算嵌入向量的欧氏距离
        dists = [[(e1 - e2).norm().item() for e2 in embeddings] for e1 in embeddings]
        df = pd.DataFrame(dists, columns=names, index=names)
        print(df)
        # 将结果保存为CSV文件
        output_path = 'face_distances.csv'
        df.to_csv(output_path)
        print(f'距离矩阵已保存到 {output_path}')

if __name__ == "__main__":
    main()